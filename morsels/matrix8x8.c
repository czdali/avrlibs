/** @file
 * @brief Library for working with 8x8 bit buffers (e.g. for a LED matrix).
 *
 * This library contains a number of function for manipulating 8x8 bit buffers.
 * Such buffers can be used for example for displaying images on LED matrices
 * of the same size. The buffers are internaly stored as arrays of 8 uint8_t.
 * The x and y coordinates used in this library start at the upper left corner.
 * @author    Dalibor Sramek, dali@insula.cz
 * @version   1.1
 * @date      2017/10/08
 * @copyright MIT License
*/

#include <avr/pgmspace.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "matrix8x8.h"

/**
Hexadecimal digits (0 - F) in a simple 3x5 font
*/
const uint8_t m88_digit[16][5] PROGMEM = {
  {0x7, 0x5, 0x5, 0x5, 0x7},  // 0
  {0x1, 0x1, 0x1, 0x1, 0x1},  // 1
  {0x7, 0x1, 0x7, 0x4, 0x7},  // 2
  {0x7, 0x1, 0x7, 0x1, 0x7},  // 3
  {0x5, 0x5, 0x7, 0x1, 0x1},  // 4
  {0x7, 0x4, 0x7, 0x1, 0x7},  // 5
  {0x7, 0x4, 0x7, 0x5, 0x7},  // 6
  {0x7, 0x1, 0x1, 0x1, 0x1},  // 7
  {0x7, 0x5, 0x7, 0x5, 0x7},  // 8
  {0x7, 0x5, 0x7, 0x1, 0x7},  // 9
  {0x0, 0x7, 0x5, 0x7, 0x5},  // A
  {0x4, 0x4, 0x7, 0x5, 0x7},  // B
  {0x7, 0x4, 0x4, 0x4, 0x7},  // C
  {0x1, 0x1, 0x7, 0x5, 0x7},  // D
  {0x7, 0x4, 0x6, 0x4, 0x7},  // E
  {0x7, 0x4, 0x6, 0x4, 0x4}   // F
};

/**
Initialize the buffer with zeroes

This function sets all bits of a given buffer to zero.
@param m is a pointer to the buffer which should be initialized
@return nothing
*/
void m8x8InitBuffer(uint8_t *m) {
  uint8_t i;
  for (i = 0; i < 8; i++) m[i] = 0;
}

/**
Copy the content of one buffer into another

This function copies the content of the first given buffer into the second
given buffer.
@param s is a pointer to the source buffer
@param t is a pointer to the target buffer
@return nothing
*/
void m8x8CopyBuffer(const uint8_t *s, uint8_t *t) {
  uint8_t i;
  for (i = 0; i < 8; i++) t[i] = s[i];
}

/**
Copy the content of a buffer located in PROGMEM into another buffer in SRAM

This function copies the content of the first given buffer which is located
into PROGMEM into the second buffer which is stored in SRAM.
@param s is a pointer to the source buffer (stored in PROGMEM)
@param t is a pointer to the target buffer (stored in SRAM)
@return nothing
*/
void m8x8CopyPgmBuffer(const uint8_t *s, uint8_t *t) {
  uint8_t i;
  for (i = 0; i < 8; i++) t[i] = pgm_read_byte(s + i);
}

/**
Compare two buffers

This function compares content of two buffers.
@param s is a pointer to the first buffer to compare
@param t is a pointer to the second buffer to compare
@return `true` if the buffers have the same content or `false` otherwise
*/
bool m8x8CmpBuffer(const uint8_t *s, const uint8_t *t) {
  bool r = true;
  uint8_t i;
  for (i = 0; i < 8; i++)
    if (s[i] != t[i]) {
      r = false;
      break;
    }
  return r;
}

/**
Initialize the buffer with random content

This function uses two 32-bit numbers from PRNG to fill the given buffer.
@param m is a pointer to the buffer which should be randomized
@return nothing
@note
This function uses two calls to `random()` which return values
from 0 to 2147483647. It means the lefmost bit is always 0. The 32nd
64th bit in the buffer will therefore always be 0.
*/
void m8x8RandomizeBuffer(uint8_t *m) {
  uint32_t *l;
  l = (uint32_t *)m;
  l[0] = random();
  l[1] = random();
}

/**
Get the value of a specified dot/bit in a buffer

This function returns the value of a dot specified by its x and y coordinates.
@param m is a pointer to the buffer
@param x is an x coordinate (0 - 7)
@param y is an y coordinate (0 - 7)
@return `true` if the dot/bit at the specified coordinates is 1 or `false`
otherwise
@note
The coordinates are truncated to 3 bits (0 - 7).
*/
bool m8x8GetDot(const uint8_t *m, uint8_t x, uint8_t y) {
  x &= 0x07;
  y &= 0x07;
  return (m[y] & (0x80 >> x)) > 0;
}

// Set dot status (buffer, x coordinate, y coordinate, new status)

/**
Set a value to a specified dot/bit in a buffer

This function sets a value to a dot specified by its x and y coordinates.
@param m is a pointer to the buffer
@param x is an x coordinate (0 - 7)
@param y is an y coordinate (0 - 7)
@param v is a value to be set (true, false)
@return nothing
@note
The coordinates are truncated to 3 bits (0 - 7).
*/
void m8x8SetDot(uint8_t *m, uint8_t x, uint8_t y, const bool v) {
  x &= 0x07;
  y &= 0x07;
  if (v)
    m[y] |= (0x80 >> x);
  else
    m[y] &= ~(0x80 >> x);
}

/**
Set the value `true` (1) to a specified dot/bit in a buffer

This function sets the value `true` (1) to a dot specified by its
x and y coordinates.
@param m is a pointer to the buffer
@param x is an x coordinate (0 - 7)
@param y is an y coordinate (0 - 7)
@return nothing
@sa m8x8SetDot
*/
void m8x8DotOn(uint8_t *m, const uint8_t x, const uint8_t y) {
  m8x8SetDot(m, x, y, true);
}

/**
Set the value `false` (0) to a specified dot/bit in a buffer

This function sets the value `false` (0) to a dot specified by its
x and y coordinates.
@param m is a pointer to the buffer
@param x is an x coordinate (0 - 7)
@param y is an y coordinate (0 - 7)
@return nothing
@sa m8x8SetDot
*/
void m8x8DotOff(uint8_t *m, const uint8_t x, const uint8_t y) {
  m8x8SetDot(m, x, y, false);
}

/**
Toggle the value of a specified dot/bit in a buffer

This function toggles the value of a dot specified by its x and y coordinates.
@param m is a pointer to the buffer
@param x is an x coordinate (0 - 7)
@param y is an y coordinate (0 - 7)
@return nothing
@sa m8x8GetDot, m8x8SetDot
*/
void m8x8DotToggle(uint8_t *m, const uint8_t x, const uint8_t y) {
  m8x8SetDot(m, x, y, !m8x8GetDot(m, x, y));
}

// Get vertical line (buffer, x coordinate)

/**
Get bit values from a vertical line specified by its x coordinate

This function returns bits from a vertical line specified by its x coordinate
in a given buffer.
@param x is an x coordinate (0 - 7)
@return `uint8_t` where each bit corresponds to a bit from the vertical line
(bit 0 in the returned value corresponds to bit [x, 0] in the buffer etc.)
*/
uint8_t m8x8GetVLine(const uint8_t *m, const uint8_t x) {
  uint8_t i, v, r = 0;
  v = 0x80 >> x;  // bit mask for x-th vertical line
  for (i = 0; i < 7; i++)
    if (m[i] & v) r |= (1 << i);
  return r;
}

/**
Scroll a buffer one line up

This function scrolls a given buffer one line up and fills the bottom line
with given values.
@param m is a pointer to the buffer which should be scrolled
@param b contains bits that should be filled into the bottom line
@return bits from the top line that is scrolled outside of the buffer
*/
uint8_t m8x8ScrollUp(uint8_t *m, const uint8_t b) {
  uint8_t i, r;
  r = m[0];
  for (i = 0; i < 7; i++) m[i] = m[i + 1];
  m[7] = b;
  return r;
}

/**
Scroll a buffer one line down

This function scrolls a given buffer one line down and fills the top line
with given values.
@param m is a pointer to the buffer which should be scrolled
@param b contains bits that should be filled into the top line
@return bits from the bottom line that is scrolled outside of the buffer
*/
uint8_t m8x8ScrollDown(uint8_t *m, const uint8_t b) {
  uint8_t i, r;
  r = m[7];
  for (i = 7; i > 0; i--) m[i] = m[i - 1];
  m[0] = b;
  return r;
}

/**
Scroll a buffer one vertical line left

This function scrolls a given buffer one vertical line left and fills
the rightmost line with given values.
@param m is a pointer to the buffer which should be scrolled
@param b contains bits that should be filled into the rightmost line
@return bits from the leftmost line that is scrolled outside of the buffer
@sa m8x8GetVLine
*/
uint8_t m8x8ScrollLeft(uint8_t *m, const uint8_t b) {
  uint8_t i, v, r = 0;
  for (i = 0; i < 7; i++) {
    v = 1 << i;               // bit mask for i-th vertical bit
    if (m[i] & 0x80) r |= v;  // store the leftmost bit
    m[i] <<= 1;               // shift all bits to the left
    if (b & v) m[i] |= 0x01;  // set the rightmost bit
  }
  return r;
}

/**
Scroll a buffer one vertical line right

This function scrolls a given buffer one vertical line right and fills
the leftmost line with given values.
@param m is a pointer to the buffer which should be scrolled
@param b contains bits that should be filled into the leftmost line
@return bits from the rightmost line that is scrolled outside of the buffer
@sa m8x8GetVLine
*/
uint8_t m8x8ScrollRight(uint8_t *m, const uint8_t b) {
  uint8_t i, v, r = 0;
  for (i = 0; i < 7; i++) {
    v = 1 << i;               // bit mask for i-th vertical bit
    if (m[i] & 0x01) r |= v;  // store the rightmost bit
    m[i] >>= 1;               // shift all bits to the right
    if (b & v) m[i] |= 0x80;  // set the leftmost bit
  }
  return r;
}

/**
Draw a horizontal line of a given length starting from a given point

This function sets a specified number of bits starting from a given point
on a horizontal line to 1.
@param m is a pointer to the buffer where the line should be drawn
@param x is the x coordinate of the starting point (0 - 7)
@param y is the y coordinate of the starting point (0 - 7)
@param l is the length of the line (number of bits set to 1)
@return nothing
@sa m8x8DotOn
*/
void m8x8DrawHLine(
  uint8_t *m, const uint8_t x, const uint8_t y, const uint8_t l) {
  uint8_t i;
  for (i = x; i < (x + l); i++) m8x8DotOn(m, i, y);
}

/**
Draw a vertical line of a given length starting from a given point

This function sets a specified number of bits starting from a given point
on a vertical line to 1.
@param m is a pointer to the buffer where the line should be drawn
@param x is the x coordinate of the starting point (0 - 7)
@param y is the y coordinate of the starting point (0 - 7)
@param l is the length of the line (number of bits set to 1)
@return nothing
@sa m8x8DotOn
*/
void m8x8DrawVLine(
  uint8_t *m, const uint8_t x, const uint8_t y, const uint8_t l) {
  uint8_t i;
  for (i = y; i < (y + l); i++) m8x8DotOn(m, x, i);
}

/**
Draw a hexadecimal digit in a simple 3x5 dots font

This function ...
@param m is a pointer to the buffer where the digit should be drawn
@param d is a hexadecimal digit to be drawn (0 - F)
@param xr is a position of the digit on the x axis (right side of the digit
from the right side of the buffer - sensible values are 0 - 5)
@param y is a position of the digit on the y axis (top side of the digit
from the top side of the buffer - sensible values are 0 - 3)
@return nothing
@attention The parameter for the digit x position is reversed.
*/
void m8x8DrawDigit(uint8_t *m, uint8_t d, uint8_t xr, uint8_t y) {
  uint8_t b, i;
  // Limit input values to safe range
  d &= 0x0f;
  y &= 0x03;
  // Place the digit byte by byte to the desired position
  for (i = y; i < y + 5; i++) {
    b = pgm_read_byte(m88_digit[d] + i);
    b <<= xr;
    m[i] |= b;
  }
}

/**
Draw a 2-digit decimal number (0 - 99)

This function ...
@param m is a pointer to the buffer where the number should be drawn
@param n is a number to be drawn (0 - 99)
@param y is a position of the number on the y axis (top side of the number
from the top side of the buffer - sensible values are 0 - 3)
@param lz specifies if leading zero should be drawn for number smaller than 10
@return nothing
*/
void m8x8DrawDecimalNumber(
  uint8_t *m, uint8_t const n, const uint8_t y, const bool lz) {
  uint8_t d;
  d = n / 10;
  if ((d > 0) || lz) m8x8DrawDigit(m, d, 4, y);
  d = n % 10;
  m8x8DrawDigit(m, d, 0, y);
}

/**
Draw a 2-digit hexadecimal number (0 - FF)

This function ...
@param m is a pointer to the buffer where the number should be drawn
@param n is a number to be drawn (0 - FF)
@param y is a position of the number on the y axis (top side of the number
from the top side of the buffer - sensible values are 0 - 3)
@param lz specifies if leading zero should be drawn for number smaller than 0x10
@return nothing
*/
void m8x8DrawHexadecimalNumber(
  uint8_t *m, uint8_t const n, const uint8_t y, const bool lz) {
  uint8_t d;
  d = n >> 4;
  if ((d > 0) || lz) m8x8DrawDigit(m, d, 4, y);
  d = n & 0x0f;
  m8x8DrawDigit(m, d, 0, y);
}
