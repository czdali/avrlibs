/** @file
 * @brief Simple demo: blinking LED with frequency determined by ADC reading
 *
 * @author    Dalibor Sramek, dali@insula.cz
 * @version   0.2
 * @date      2017/10/08
 * @copyright MIT License
 * @note Ready for an ATtiny85 at 8MHz.
*/

#include <avr/io.h>
#include "common.h"

/** @cond DEMODOC */
#define BLINK_PORT PORTB  // port to use
#define BLINK_DDR DDRB    // port direction register
#define BLINK_PIN PB4     // pin to use (where the LED is connected)
#define ADC_ADMUX 0x00    // initial ADMUX value (reference voltage = Vcc)
#define ADC_PSC (ADPS1 | ADPS2)  // ADC presacler = 64 (ATtiny85)
#define ADC_MUXCH 0x03           // ADC channel
/** @endcond */

int main() {
  uint16_t pause;
  SBI(BLINK_DDR, PB4);           // set the pin to output mode
  ADMUX = ADC_ADMUX;             // set voltage reference
  ADCSRA |= ADC_PSC;             // set ADC prescaler
  SBI(ADCSRA, ADEN);             // enable ADC
  while (1) {                    // do infinitely
    TBI(BLINK_PORT, PB4);        // toggle pin value
    pause = readADC(ADC_MUXCH);  // read a value from ADC
    delay(pause);                // wait
  }
}
