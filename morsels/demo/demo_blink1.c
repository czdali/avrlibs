/** @file
* @brief Simple demo: blinking LED
*
* @author    Dalibor Sramek, dali@insula.cz
* @version   0.2
* @date      2017/10/08
* @copyright MIT License
* @note Ready for an ATtiny85 at 8MHz.
*/

#include <avr/io.h>
#include "common.h"

/** @cond DEMODOC */
#define BLINK_PORT PORTB  // port to use
#define BLINK_DDR DDRB    // port direction register
#define BLINK_PIN PB4     // pin to use (where the LED is connected)
#define PAUSE 500         // 1/2 of the blinking period in ms
/** @endcond */

int main() {
  SBI(BLINK_DDR, PB4);     // set the pin to output mode
  while (1) {              // do infinitely
    TBI(BLINK_PORT, PB4);  // toggle pin value
    delay(PAUSE);          // wait
  }
}
