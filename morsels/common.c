/** @file
 * @brief Library with common utility functions for AVR programming.
 *
 * This library contains a number of functions that help with AVR programming.
 * Some of the functions emulate Arduino API.
 * @author    Dalibor Sramek, dali@insula.cz
 * @version   1.2
 * @date      2017/10/20
 * @copyright MIT License
*/

#include <avr/io.h>
#include <stdint.h>
#include <util/delay.h>

#include "common.h"

/* --- Arithmetic functions --- */

/**
Calculate the absolute value of a 16-bit integer

This function implements efficient absolute value calculation for 16-bit
integers. It does not use conditional branching.
@param v is a 16-bit integer value which may be positive or negative
@return absolute value of the given integer
*/
int16_t u16Abs(const int16_t v) {
  int16_t x = v >> 15;
  return ((v ^ x) - x);
}

/**
Convert an unsigned 16 bit integer into unpacked BCD in reverse order

This function converts an 16-bit unsigned integer into unpacked BCD in reverse
order.
For example `(uint16_t)1234` becomes `0x04, 0x03, 0x02, 0x01, 0x00`
if the parameter `c` is set to `0`. This format can be directly copied e.g.
to a MAX7219 controlled display with BCD decode mode switched on.
@param b is a pointer to a byte buffer where the result of the conversion
should be stored
@param v is an integer value that should be converted
@param c is a character that should be used instead of leading zeroes
in the converted buffer; use `BCD_NOFILL` to leave the places of leading zeroes
untouched in the buffer
@return a number of valid decimal digits stored in the buffer
@note
The caller is responsible for allocation of a buffer with sufficient size.
The size must be equal or large than the number of decimal digits that is
needed to record the given integer including optional leading characters.
*/
uint8_t u16ToURBCD(uint8_t *b, uint16_t v, const uint8_t c) {
  uint8_t n = 0;  // number of valid digits (return value)
  uint8_t m = 4;  // current digit position in the buffer
  int8_t i;       // digit counter
  // First we need to handle large numbers specifically because further
  // processing depends on checking the MSB
  if (v & 0x8000) {
    v -= 30000;
    i = 2;
  } else
    i = -1;
  // Count 10000s
  do
    i++;
  while (!((v -= 10000) & 0x8000));
  if (i || n) n++;
  if (i)
    b[m] = i;
  else if (c != BCD_NOFILL)
    b[m] = c;
  m--;
  // Count 1000s
  i = 10;
  do
    i--;
  while ((v += 1000) & 0x8000);
  if (i || n) n++;
  if (i)
    b[m] = i;
  else if (c != BCD_NOFILL)
    b[m] = c;
  m--;
  // Count 100s
  i = -1;
  do
    i++;
  while (!((v -= 100) & 0x8000));
  if (i || n) n++;
  if (i)
    b[m] = i;
  else if (c != BCD_NOFILL)
    b[m] = c;
  m--;
  // Count 10s
  i = 10;
  do
    i--;
  while ((v += 10) & 0x8000);
  if (i || n) n++;
  if (i)
    b[m] = i;
  else if (c != BCD_NOFILL)
    b[m] = c;
  // The rest are 1s
  b[0] = v;
  return (n + 1);
}

/**
Scale a 10-bit integer (0 - 1023) to the 0 - 1000 range

This function takes a 10-bit number (0 - 1023) and scales it to the 0 - 1000
range. It can be used for converting ADC readings to decimal representations.
@param n is a 10-bit number that should be scaled
@return a scaled value in the 0 - 1000 range
*/
uint16_t scale10BitsToDec(uint16_t n) {
  int m = n;
  m >>= 6;  // n divided by 64
  n -= m;
  m >>= 1;  // n divided by 128
  n -= m;
  m >>= 2;  // n divided by 512
  n -= m;
  return (n);
}

/* --- I/O functions --- */

/**
Read a value from a given channel of ADC

This function starts an ADC conversion on a given ADC channel (ADC<ch>)
and waits until the conversion is finished. Then the function returns
the ADC value.
@param ch is the channel which should be used for the conversion
@return ADC value from the given channel after the conversion is finished
@note
It is assumed that the ADC channel is selected by setting at most 4 least
significant bits in the ADMUX register. (This is true for ATtiny 13 (only 2
bits), 25, 45, 85 and ATmega 8, 16, and 32. Check datasheets for other MCUs.)
@see
This function is adapted from _Make: AVR Programming_ by by Elliot Williams
(page 148).
*/
uint16_t readADC(uint8_t ch) {
  ADMUX = (ADMUX & 0xf0) | ch;  // set channel
  // ADCSRA |= (1 << ADSC);                  // start conversion
  SBI(ADCSRA, ADSC);
  loop_until_bit_is_clear(ADCSRA, ADSC);  // wait for the conversion to finish
  return ADC;
}

/**
Send a byte serially out in the MSB order using data and clock pins

This function iterates over the bits in the given byte in the MSB order.
It sets the appropriate status of the data pin for each bit and then send
it out by generating a pulse (low-high-low) on the clock pin.
@param *dpo is a data port
@param dpi is a data pin number
@param *cpo is a clock port
@param cpi is a clock pin number
@param value is a byte value that should be send out over the pins
@return nothing
@see https://www.arduino.cc/en/Reference/ShiftOut
*/
void shiftOutMSB(
  const volatile uint8_t *dpo, const uint8_t dpi, const volatile uint8_t *cpo,
  const uint8_t cpi, uint8_t v) {
  uint8_t i;
  // Ensure clock is low in the beginning
  CBI(*cpo, cpi);
  // Iterate over bits
  for (i = 0; i < 8; i++) {
    // Set the data pin according to the value of the MSB
    if (v & 0b10000000)
      SBI(*dpo, dpi);
    else
      CBI(*dpo, dpi);
    // Shift to the next bit
    v <<= 1;
    // Generate a clock pulse
    SBI(*cpo, cpi);
    CBI(*cpo, cpi);
  }
}

/* --- System functions --- */

/**
Simple replacement for Arduino `delay()`

This function does nothing for a specified number of milliseconds.
@param ms is a number of milliseconds to do nothing
@return nothing
@warning
This function is unsuitable for time oriented operations with any need
for precision. It simply call `_delay_ms(1)` in a loop.
@see https://www.arduino.cc/en/Reference/Delay
*/
void delay(const uint16_t ms) {
  uint16_t i;
  for (i = 0; i < ms; i++) _delay_ms(1);
}

/**
Get number of available bytes in SRAM

This function calculates the number of available bytes in SRAM at the moment.
@param none
@return number of available bytes in SRAM
@note Adapted from code at http://playground.arduino.cc/Code/AvailableMemory
*/
uint16_t freeRAM() {
  extern uint16_t __heap_start, *__brkval;
  uint16_t ram;
  ram = (uint16_t)&ram;
  if (__brkval == 0)
    ram -= (uint16_t)&__heap_start;
  else
    ram -= (uint16_t)__brkval;
  return ram;
}
