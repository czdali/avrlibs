# avrlibs #

Pure C libraries for AVR MCUs.

At the moment there are three libraries:

* common - utility functions often inspired by Arduino library
* MAX7219 - library to communicate with displays controlled by a MAX7219
or MAX7221
* [SSLD](ssld/README.md) - library to communicate with displays controlled
by 74HC595 shift registers (Seven Segment LED Displays)
