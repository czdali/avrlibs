/** @file
 * @brief MAX7219 / MAX7221 library for LED segment displays or LED matrices
 *
 * @author    Dalibor Sramek, dali@insula.cz
 * @version   1.0
 * @date      2017/09/29
 * @copyright MIT License
 * @note
 * Inspired by an old MAX7219 module from Randy Rasa
 * http://ee.cleversoul.com/max7219-source.html
 * @see https://datasheets.maximintegrated.com/en/ds/MAX7219-MAX7221.pdf
*/

#include <avr/pgmspace.h>
#include <stdbool.h>
#include <stdint.h>

#include "common.h"
#include "max7219.h"

/** "decode mode" register number */
#define REG_DECODE 0x09

/** "brightness" register number */
#define REG_BRIGHTNESS 0x0a

/** "scan limit" register number */
#define REG_SCAN_LIMIT 0x0b

/** "shutdown" register number */
#define REG_SHUTDOWN 0x0c

/** "display test" register number */
#define REG_TEST 0x0f

/** minimum display brightness */
#define BRIGHTNESS_MIN 0x00

/** maximum display brightness */
#define BRIGHTNESS_MAX 0x0f

// Private function declarations
static void max7219_send_byte(uint8_t data);

// Bit masks defining various digits and characters that can be displayed
// in the not BCD-decoded mode
const uint8_t MAX_DIGIT_BITS[] PROGMEM = {
  0x7e, 0x30, 0x6d, 0x79, 0x33, 0x5b, 0x5f, 0x70, 0x7f,
  0x7b, 0x77, 0x1f, 0x4e, 0x3d, 0x4f, 0x47, 0x00, 0x01,
  0x08, 0x09, 0x37, 0x38, 0x0e, 0x07, 0x67, 0x3e, 0x3b};

/**
Initialize MAX7219 module - must be called before any other MAX7219 functions

This function configures communication pins and set initial operating
parameters to a display. It sets the scan limit to maximum, disables
shutdown and test modes, clears the display and set brightness to maximum.
@param none
@return nothing
@note
On initial power-up, all control registers are reset, the display is blanked,
and the MAX7219/MAX7221 enter shutdown mode. It will initially be set to scan
one digit, it will not decode data in the data registers, and the intensity
register will be set to its minimum value.
*/
void max7219_init() {
  SBI(MAX7219_DIN_DDR, MAX7219_DIN_PIN);   // configure DIN as output
  SBI(MAX7219_CLK_DDR, MAX7219_CLK_PIN);   // configure CLK  as output
  SBI(MAX7219_CS_DDR, MAX7219_CS_PIN);     // configure CS as output
  SBI(MAX7219_CS_PORT, MAX7219_CS_PIN);    // CS will be high by default
  max7219_shutdown(false);                 // disable shutdown mode
  max7219_test(false);                     // disable test mode
  max7219_scan(7);                         // scan all eight digits
  max7219_brightness(BRIGHTNESS_MAX / 2);  // set to 50% intensity
  max7219_decode(0);                       // set to "no decode" for all digits
  max7219_clear();                         // clear all digits
}

/**
Control MAX7219 shutdown mode

This function toggles the _shutdown mode_ of a display controlled by a MAX7219.
@param status is either `true` to switch the _shutdown mode_ on or `false`
to switch it off
@return nothing
@note
When the MAX7219 is in shutdown mode, the scan oscillator is halted, all
segment current sources are pulled to ground, and all digit drivers are pulled
to V+, thereby blanking the display. The MAX7221 is identical, except
the drivers are high-impedance. Data in the digit and control registers remains
unaltered. Shutdown can be used to save power or as an alarm to flash
the display by successively entering and leaving shutdown mode. For minimum
supply current in shutdown mode, logic inputs should be at ground or V+
(CMOS-logic levels).
Typically, it takes less than 250μs for the MAX7219/MAX7221 to leave shutdown
mode. The display driver can be programmed while in shutdown mode, and shutdown
mode can be overridden by the display-test function.
*/
void max7219_shutdown(const bool status) {
  if (status)
    max7219_send_word(REG_SHUTDOWN, 0);  // put MAX7219 into "shutdown" mode
  else
    max7219_send_word(REG_SHUTDOWN, 1);  // put MAX7219 out of "shutdown" mode
}

/**
Control MAX7219 test mode

This function sets toggles the _test mode_ of a display controlled by
a MAX7219.
@param status is either `true` to switch the _test mode_ on or `false`
to switch it off
@return nothing
@note
Display-test mode turns all LEDs on by overriding, but not altering, all
controls and digit registers (including the shutdown register). In display-test
mode, 8 digits are scanned and the duty cycle is 31/32 (15/16 for MAX7221).
*/
void max7219_test(const bool status) {
  if (status)
    max7219_send_word(REG_TEST, 1);  // put MAX7219 into "display test" mode
  else
    max7219_send_word(REG_TEST, 0);  // put MAX7219 out of "display test" mode
}

/**
Set scan limit (how many digits are displayed)

The scan-limit register sets how many digits are displayed from 1 to 8.
@param sr determines the scan range
@return nothing
@note
Digits are displayed in a multiplexed manner with a typical display scan rate
of 800Hz with 8 digits displayed. If fewer digits are displayed, the scan rate
is 8fOSC/N, where N is the number of digits scanned. Since the number
of scanned digits affects the display brightness, the scan-limit register
should not be used to blank portions of the display (such as leading zero
suppression).
*/
void max7219_scan(uint8_t sr) {
  sr &= 0x07;                             // mask off extra bits
  max7219_send_word(REG_SCAN_LIMIT, sr);  // set decode mode
}

/**
Set display brightness

This function sets brightness level of a display controlled by a MAX7219.
@param brightness is the desired brightness level for the display (0 - 15)
@return nothing
@note
Digital control of display brightness is provided by an internal pulse-width
modulator, which is controlled by the lower nibble of the intensity register.
The modulator scales the average segment current in 16 steps.
*/
void max7219_brightness(uint8_t brightness) {
  brightness &= 0x0f;                             // mask off extra bits
  max7219_send_word(REG_BRIGHTNESS, brightness);  // set brightness
}

/**
Set decode mode for digits

The decode-mode register sets BCD code (0-9, E, H, L, P, and -) or no-decode
operation for each digit. Each bit in the register corresponds to one digit.
A logic high selects code B decoding while logic low bypasses the decoder.
@param dm determines the decode mode for display digits - one bit for each
digit
@return nothing
@note
When the decode mode is used, the decoder looks only at the lower nibble
of the data in the digit registers (D3 - D0), disregarding bits D4 - D6.
D7, which sets the decimal point (SEG DP), is independent of the decoder and is
positive logic (D7 = 1 turns the decimal point on).
*/
void max7219_decode(const uint8_t dm) {
  max7219_send_word(REG_DECODE, dm);  // set decode mode
}

/**
Brief function description

This function ...
@param x
@return x
*/
uint8_t max7219_get_bits(uint8_t c) {
  return pgm_read_byte(MAX_DIGIT_BITS + c);
}

/**
Fill the display with a given byte

This function fills all display digits with a given byte.
@param b is the byte which should be used to fill the display
@return nothing
*/
void max7219_fill(uint8_t b) {
  uint8_t i;
  // Send the byte to each digit
  for (i = 1; i < 9; i++) max7219_send_word(i, b);
}

/**
Clear the display (turn all segments/LEDs off)

This function turns all addressable display elements (LED segments or LEDs) off.
@param none
@return nothing
*/
void max7219_clear() {
  max7219_fill(0);
}

/**
Copy a byte buffer to a display

This function sends bytes from a byte buffer to a MAX7219 controlled display.
@param b is a pointer to a buffer which should be displayed
@return nothing
*/
void max7219_display(uint8_t *b, uint8_t l) {
  uint8_t i;
  // There is only 8 addressable display bytes
  l = l <= 8 ? l : 8;
  // Just copy the buffer byte by byte to the digit registers
  for (i = 0; i < l; i++) max7219_send_word(i + 1, b[i]);
}

/**
Send a word (two bytes) to MAX7219

This function sends a word of data serially to an attached MAX7219. It uses
data and clock pins.
@param reg is the register number of the MAX7219 to which the data should be
written
@param data is the data byte to be sent to the specified register of the MAX7219
@return nothing
@note
The communication with a MAX7219 entails sending 2-byte commands. The first byte
is a number of a control register in the MAX7219 and the second byte is
the value. Registers with numbers 1 - 8 represent digits from 8 LED segments
or 8 LED lines on matrix displays. Registers with higher numbers are control
registers.
For the MAX7219, serial data at DIN, sent in 16-bit packets, is shifted into
the internal 16-bit shift register with each rising edge of CLK regardless
of the state of LOAD. For the MAX7221, CS must be low to clock data in or out.
The data is then latched into either the digit or control registers
on the rising edge of LOAD/CS. LOAD/CS must go high concurrently with or after
the 16th rising clock edge, but before the next rising clock
edge or data will be lost.
*/
void max7219_send_word(const uint8_t reg, const uint8_t data) {
  CBI(MAX7219_CS_PORT, MAX7219_CS_PIN);  // take CS low to begin transmission
  max7219_send_byte(reg);                // first send the register number
  max7219_send_byte(data);               // send the data byte
  SBI(MAX7219_CS_PORT, MAX7219_CS_PIN);  // take CS back to high
}

/**
Send a byte to MAX7219

This function sends a byte of data serially to an attached MAX7219. It uses
data and clock pins.
@param data is the data byte to be sent to the MAX7219
@return nothing
@note
This function is private because it serves just to implement the more useful
max7219_send_word()
*/
static void max7219_send_byte(const uint8_t data) {
  shiftOutMSB(
    &MAX7219_DIN_PORT, MAX7219_DIN_PIN, &MAX7219_CLK_PORT, MAX7219_CLK_PIN,
    data);
}
