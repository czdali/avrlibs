/** @file
* @brief Simple demo: Button press counter on a MAX7219 7-segment LED display
*
* This demo displays a counter on a MAX7219 7-segment LED display in BCD mode.
* It counts button presses and uses pin change interrupt.
* @author    Dalibor Sramek, dali@insula.cz
* @version   0.1
* @date      2017/11/03
* @copyright MIT License
* @note
* Ready for an ATtiny85 at 8MHz. The button and interrupt definitions
* must be modified for other MCUs.
*/

// Define where the button is connected (connect the other button pin to GND)
#define BUTTON_PORT PORTB
#define BUTTON_PIN PINB
#define BUTTON_INT PCINT3
#define BUTTON PB3

#include <avr/interrupt.h>
#include <avr/io.h>

#include "common.h"
#include "max7219.h"

// The counter will be changed in an ISR
volatile uint16_t cnt = 0;

// ISR increments the counter if the button is pressed down
ISR(PCINT0_vect) {
  if (bit_is_clear(BUTTON_PIN, BUTTON)) cnt++;
}

// Interrupt initialization
void initInterrupt() {
  GIMSK |= (1 << PCIE);        // enable pin-change interrupt
  PCMSK |= (1 << BUTTON_INT);  // set mask to look for
  sei();                       // set (global) interrupt enable bit
}

int main() {
  uint8_t buffer[8], i;
  // Initialize the LED display
  max7219_init();
  max7219_brightness(2);
  max7219_decode(0xff);
  // Initialize the display buffer with blanks
  for (i = 0; i < 8; i++) buffer[i] = MAX7219_BCD_BLANK;
  // Count and display number of presses
  BUTTON_PORT |= (1 << BUTTON);  // setup pullup resistor for the pin
  initInterrupt();
  while (1) {
    u16ToURBCD(buffer, cnt, MAX7219_BCD_0);  // use leading zeroes
    max7219_display(buffer, 5);              // there are 5 digits at most
    delay(100);
  }
}
