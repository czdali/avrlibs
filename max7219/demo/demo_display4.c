/** @file
* @brief
* Simple demo: Rotary encoder driven counter on a MAX7219 7-segment LED display
*
* This demo displays a counter on a MAX7219 7-segment LED display in BCD mode.
* The counter is driven by a rotary encoder and uses pin change interrupt.
* @author    Dalibor Sramek, dali@insula.cz
* @version   0.1
* @date      2017/11/04
* @copyright MIT License
* @note
* Ready for an ATtiny85 at 8MHz. The pins and interrupt definitions
* must be modified for other MCUs.
* @warning
* Correct function of this demo depends on proper hardware debouncing
* of the rotary encoder outputs.
*/

// Define where the encoder output is connected
#define ENCODER_PORT PORTB
#define ENCODER_PIN PINB
#define ENCODER_INT1 PCINT3
#define ENCODER1 PB3
#define ENCODER2 PB4

#include <avr/interrupt.h>
#include <avr/io.h>

#include "common.h"
#include "max7219.h"

// The counter will be changed in an ISR
volatile int16_t cnt = 0;

// ISR increments or decrements the counter according to the rotary encoder
// rotation
ISR(PCINT0_vect) {
  if (
    (bit_is_clear(ENCODER_PIN, ENCODER1)) &&
    (bit_is_set(ENCODER_PIN, ENCODER2)))
    cnt++;
  else if (
    (bit_is_set(ENCODER_PIN, ENCODER1)) &&
    (bit_is_clear(ENCODER_PIN, ENCODER2)))
    cnt++;
  else if (
    (bit_is_clear(ENCODER_PIN, ENCODER1)) &&
    (bit_is_clear(ENCODER_PIN, ENCODER2)))
    cnt--;
  else if (
    (bit_is_set(ENCODER_PIN, ENCODER1)) && (bit_is_set(ENCODER_PIN, ENCODER2)))
    cnt--;
}

// Interrupt initialization
void initInterrupt() {
  GIMSK |= (1 << PCIE);          // enable pin-change interrupt
  PCMSK |= (1 << ENCODER_INT1);  // set mask to look for (one encoder output)
  sei();                         // set (global) interrupt enable bit
}

int main() {
  uint8_t buffer[8], i;
  uint16_t abscnt;
  // Initialize the LED display
  max7219_init();
  max7219_brightness(2);
  max7219_decode(0xff);
  // Initialize the display buffer with blanks
  for (i = 0; i < 8; i++) buffer[i] = MAX7219_BCD_BLANK;
  // Count and display number base on the rotary encoder movement
  ENCODER_PORT |= (1 << ENCODER1);  // setup pullup resistor for pin 1
  ENCODER_PORT |= (1 << ENCODER2);  // setup pullup resistor for pin 2
  initInterrupt();
  while (1) {
    abscnt = u16Abs(cnt);  // we need a positive number for BCD conversion
    u16ToURBCD(buffer, abscnt, MAX7219_BCD_BLANK);  // leading blanks
    if (cnt < 0)
      buffer[7] = MAX7219_BCD_MINUS;  // add minus sign for a negative number
    else
      buffer[7] = MAX7219_BCD_BLANK;
    max7219_display(buffer, 8);  // display whole buffer
    delay(100);
  }
}
