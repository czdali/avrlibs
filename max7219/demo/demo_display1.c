/** @file
* @brief Simple demo: Counter on a MAX7219 7-segment LED display in BCD mode
*
* @author    Dalibor Sramek, dali@insula.cz
* @version   0.1
* @date      2017/10/20
* @copyright MIT License
* @note Ready for an ATtiny85 at 8MHz.
*/

#include "max7219.h"

#include "common.h"

int main() {
  uint8_t buffer[8];
  uint16_t i;
  // Initialize the LED matrix
  max7219_init();
  max7219_brightness(2);
  max7219_decode(0xff);
  // Initialize the display buffer with blanks
  for (i = 0; i < 8; i++) buffer[i] = MAX7219_BCD_BLANK;
  // Count and display
  i = 0;
  while (1) {
    u16ToURBCD(buffer, i, MAX7219_BCD_0);  // use leading zeroes
    max7219_display(buffer, 5);            // there are 5 digits at most
    delay(100);
    i++;  // this will overflow after 65535 and start again from 0
  }
}
