/** @file
* @brief Simple demo: Display output from the internal temperature sensor
*
* @author    Dalibor Sramek, dali@insula.cz
* @version   0.1
* @date      2017/10/20
* @copyright MIT License
* @note Ready for an ATtiny85 at 8MHz.
* The temperature measurement is based on an on-chip temperature sensor
* that is coupled to a single ended ADC4 channel. Selecting the ADC4 channel
* by writing the MUX[3:0] bits in ADMUX register to 1111 enables
* the temperature sensor. The internal 1.1V reference must also be selected
* for the ADC reference source in the temperature sensor measurement.
* When the temperature sensor is enabled, the ADC converter can be used in
* single conversion mode to measure the voltage over the temperature sensor.
* @see MCU datasheet: chapter Temperature Measurement
*/

#include "common.h"
#include "max7219.h"

/** @cond DEMODOC */
#define ADC_ADMUX 0b10000000     // ADMUX value (reference voltage = 1.1V)
#define ADC_PSC (ADPS1 | ADPS2)  // ADC presacler = 64 (ATtiny85)
#define ADC_MUXCH 0b00001111     // ADC channel - temperature sensor
/** @endcond */

int main() {
  uint8_t buffer[10];
  uint16_t i, v, t;
  // Initialize ADC
  ADMUX = ADC_ADMUX;  // set voltage reference
  ADCSRA |= ADC_PSC;  // set ADC prescaler
  SBI(ADCSRA, ADEN);  // enable ADC
  // Initialize the LED matrix
  max7219_init();
  max7219_brightness(2);
  max7219_decode(0xff);
  // Initialize the display buffer with blanks
  for (i = 0; i < 8; i++) buffer[i] = MAX7219_BCD_BLANK;
  // Read and display temperature values
  while (1) {
    v = readADC(ADC_MUXCH);  // read a value from ADC
    t = v > 275 ? v - 275 : 0;             // temperature approximation
    // Put the value from ADC to the right side of the display buffer
    u16ToURBCD(buffer, v, MAX7219_BCD_0);  // use leading zeroes
    buffer[4] = MAX7219_BCD_MINUS;         // separate 2 parts of the display
    // The temperature goes to the left side
    u16ToURBCD(buffer + 5, t, MAX7219_BCD_BLANK);  // use leading blanks
    // Display the buffer
    max7219_display(buffer, 8);
    delay(500);
  }
}
