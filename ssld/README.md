# SSLD #

SSLD is a library allowing AVR MCUs to display data on 7 segment LED displays
with 74HC595 shift registers.

You can buy cheap 4 digit displays on auction sites for prices only sligthly
higher than 1 USD. (You can see two 74HC595 shift registers from backside.)

![Front](images/ssld4a.jpg)
![Back](images/ssld4b.jpg)

### Library Usage ####
```
git clone https://bitbucket.org/czdali/avrlibs.git
cd avrlibs/ssld
```
Edit `ssld_config.h` and define which pins are connected to a display.
```
cd demo1
```
Edit *Makefile*. By default it is configured for an ATtiny85 and a USBtiny
programmer.
```
make
make flash
```
To use in your own programs copy `ssld_config.h`, `ssld.h` and `ssld.c`
to your project directory. The API is simple enough to be learned from reading
the header file and demo sources.

To display a 16 bit unsigned integer:
```c
uint8_t buffer[4];          // display buffer - 1 byte for each digit
uint16_t value = 1234;      // value to be displayed
ssld_dec16(buffer, value);  // display the value as a decimal number
while (1)
  ssld_refresh(buffer);     // refresh display until power is switched off
  _delay_us(1000);
}
```
