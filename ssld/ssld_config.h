/*
 * 7 segment LED display library for displays with 74HC595 shift registers.
 * Copyright (c) 2017 Dalibor Sramek, http://www.insula.cz
 * v1.0
*/

#include <avr/io.h>

// Number of digits on the display (and assumed size of a display buffer)
#define SSLD_SIZE 4

// Ports and respective pins where DIO, SCLK and RCLK are connected
// Do not forget to set the pins to output mode in your code
#define SSLD_DIO_PORT PORTB
#define SSLD_DIO_PIN PB0
#define SSLD_SCLK_PORT PORTB
#define SSLD_SCLK_PIN PB2
#define SSLD_RCLK_PORT PORTB
#define SSLD_RCLK_PIN PB1
