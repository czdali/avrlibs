/*
 * 7 segment LED display library for displays with 74HC595 shift registers.
 * Copyright (c) 2017 Dalibor Sramek, http://www.insula.cz
 * DEMO 1
*/

#include <avr/io.h>
#include <util/delay.h>
#include "ssld.h"

// A LED display must be periodically refreshed
// It can be done from an ISR but to keep the demo simple just call the refresh
// function from a cycle
void show(uint8_t *buffer, uint16_t cycles) {
  for (uint16_t i = 0; i < cycles; i++) {
    ssld_refresh(buffer);
    _delay_us(1000);  // all 4 digits are displayed every 4 ms = 250 Hz
  }
}

int main(void) {
  uint8_t buffer[4];  // display buffer - 1 byte for each digit

  // Set pins 0 - 2 of PORTB to output mode
  //       SCLK        RCLK        DIO
  DDRB |= _BV(DDB0) | _BV(DDB1) | _BV(DDB2);

  // Main loop
  while (1) {
    // Firts display all possible digits on the whole display
    for (uint8_t i = 0; i <= DG_BLANK; i++) {
      for (uint8_t j = 0; j < SSLD_SIZE; j++) ssld_set_digit(buffer, j, i);
      show(buffer, 1000);  // 1 second for each digit
    }

    // Now the display is all blank
    // Show a crude animation by switching individual segments
    for (uint8_t n = 0; n <= 5; n++) {
      for (uint8_t i = 0; i <= SSLD_SIZE; i++) {
        for (uint8_t j = 0; j < 6; j++) {
          ssld_segment_on(buffer, i, j);
          if (j > 0)
            ssld_segment_off(buffer, i, j - 1);
          else if (i > 0)
            ssld_segment_off(buffer, i - 1, 5);
          show(buffer, 100);  // 0.1 second
        }
      }
    }

    // Quickly count from 1 to 1000 and show the counter as a decimal number
    for (uint16_t i = 1; i <= 1000; i++) {
      ssld_dec16(buffer, i);
      show(buffer, 100);  // 0.1 second
    }
  }

  return (0);
}
