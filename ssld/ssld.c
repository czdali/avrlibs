/*
 * 7 segment LED display library for displays with 74HC595 shift registers.
 * Copyright (c) 2017 Dalibor Sramek, http://www.insula.cz
 * v1.0
*/

#include <avr/pgmspace.h>
#include "ssld.h"

// Digits "font" definition 0-9, A-F, -, H, L, o, r, <blank>
// Bits are inverted - zero bit means an illuminated segment
const uint8_t SSLD_DIGITS[] PROGMEM = {
  0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 0x80, 0x90, 0x88,
  0x83, 0xC6, 0xA1, 0x86, 0x8e, 0xbf, 0x89, 0xc7, 0xa3, 0xaf, 0xff};

// Functions for hardware communications

// Send out 8 bits to DIO
// Bits may represent segments of one digit or a digit address number
void ssld_send_bits(uint8_t bits) {
  // Iterate over the 8 bits, MSB first
  for (uint8_t i = 8; i >= 1; i--) {
    if (bits & 0x80)
      SSLD_DIO_PORT |= _BV(SSLD_DIO_PIN);  // set DIO high
    else
      SSLD_DIO_PORT &= ~_BV(SSLD_DIO_PIN);  // set DIO low
    // Send the bit
    SSLD_SCLK_PORT &= ~_BV(SSLD_SCLK_PIN);  // set SCLK low
    SSLD_SCLK_PORT |= _BV(SSLD_SCLK_PIN);   // set SCLK high
    // Shift to get the next one
    bits <<= 1;
  }
}

// Latch previously sent bits and illuminate one digit
void ssld_latch_digit(const uint8_t dignum) {
  ssld_send_bits(dignum);                 // send the digit address number
  SSLD_RCLK_PORT &= ~_BV(SSLD_RCLK_PIN);  // set RCLK low
  SSLD_RCLK_PORT |= _BV(SSLD_RCLK_PIN);   // set RCLK high
}

// Cycle over a byte buffer and display one digit for one call
void ssld_refresh(uint8_t *buffer) {
  static uint8_t i = 0;
  ssld_send_bits(buffer[i]);  // display the current digit
  ssld_latch_digit(1 << i);   // digit address is encoded as (2 ** addr)
  i++;                        // move the pointer

  if (i >= SSLD_SIZE) i = 0;  // cycle over
}

// Functions to control individual segments

// Switch an individual display segment off
void ssld_segment_off(
  uint8_t *buffer, const uint8_t dignum, const uint8_t segnum) {
  buffer[dignum] |= (1 << segnum);
}

// Switch an individual display segment on
void ssld_segment_on(
  uint8_t *buffer, const uint8_t dignum, const uint8_t segnum) {
  buffer[dignum] &= ~(1 << segnum);
}

// Functions that help in displaying digits and numbers

// Convert a digit code to the actual displayed bits/segments
uint8_t ssld_segments(const uint8_t i) {
  return (pgm_read_byte(&SSLD_DIGITS[i]));  // read from data in program memory
}

// Set one digit
void ssld_set_digit(
  uint8_t *buffer, const uint8_t dignum, const digit_t digit) {
  buffer[dignum] = ssld_segments(digit);
}

// Render 16 bit integer into a 4-byte display buffer as hexadecimal
void ssld_hexa16(uint8_t *buffer, const uint16_t number) {
  buffer[0] = ssld_segments(number & 0x0f);
  buffer[1] = ssld_segments((number >> 4) & 0x0f);
  buffer[2] = ssld_segments((number >> 8) & 0x0f);
  buffer[3] = ssld_segments((number >> 12) & 0x0f);
}

// Render 16 bit integer into a 4-byte display buffer as decimal (0 - 9999)
void ssld_dec16(uint8_t *buffer, uint16_t number) {
  uint8_t i = 0;

  while (number >= 1000) {
    number -= 1000;
    i++;
  }

  if (i < 10)
    buffer[3] = ssld_segments(i);
  else
    buffer[3] = ssld_segments(0x0e);

  i = 0;

  while (number >= 100) {
    number -= 100;
    i++;
  }

  buffer[2] = ssld_segments(i);
  i = 0;

  while (number >= 10) {
    number -= 10;
    i++;
  }

  buffer[1] = ssld_segments(i);
  buffer[0] = ssld_segments(number);
}

// Convert a 16-bit number from 0 - 1024 to 0 - 1000 range
uint16_t ssld_scale_k(uint16_t n) {
  int m = n;
  m >>= 6;  // n divided by 64
  n -= m;
  m >>= 1;  // n divided by 128
  n -= m;
  m >>= 2;  // n divided by 512
  n -= m;
  return (n);
}
