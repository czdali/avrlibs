/*
 * 7 segment LED display library for displays with 74HC595 shift registers.
 * Copyright (c) 2017 Dalibor Sramek, http://www.insula.cz
 * DEMO 2
 * Uses ADC to measure voltage and displays the result as a fraction
 * of Vref on a 4-digit display.
*/

#include <avr/io.h>
#include <util/delay.h>
#include "ssld.h"

// ADC setup
void init_adc() {
  // These settings are valid for ATtiny85
  // Adjust according to a datasheet to your MCU

  // Reference voltage source is Vcc
  // 10 bit precision (no ADLAR)
  // ADC on pin PB4
  ADMUX = 0b00000010;

  // Enable ADC and set the prescaler to 128
  ADCSRA = _BV(ADEN) | _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0);
}

int main(void) {
  uint8_t buffer[4];   // display buffer - 1 byte for each digit
  uint16_t value = 0;  // displayed value
  uint16_t nval = 0;   // new value read from ADC

  // set pins 0 - 2 of PORTB for output to enable display
  //       SCLK        RCLK        DIO
  DDRB |= _BV(DDB0) | _BV(DDB1) | _BV(DDB2);

  // Initialize ADC
  init_adc();

  // Main loop - take a measurement from ADC and display the number
  while (1) {
    ADCSRA |= (1 << ADSC);  // start ADC measurement
    while (ADCSRA & (1 << ADSC))
      ;          // wait untill the conversion complete
    nval = ADC;  // read a new value

    // Calculate kind of moving average to smooth the measurement
    value = (15 * value + nval) / 16;

    // Scale the number into 0-1000 range and put it in the buffer
    ssld_dec16(buffer, ssld_scale_k(value));
    // Display decimal point after the first digit from left
    ssld_segment_on(buffer, 3, 7);
    // Display all digits
    for (uint8_t i = 0; i < 4; i++) {
      ssld_refresh(buffer);
      _delay_us(1000);
    }
  }

  return (0);
}
