/*
 * 7 segment LED display library for displays with 74HC595 shift registers.
 * Copyright (c) 2017 Dalibor Sramek, http://www.insula.cz
 * v1.0
*/

#ifndef SSLD_H
#define SSLD_H

// Include pins configuration and other parameters
#include "ssld_config.h"

// Predefined digits
typedef enum {
  DG_0,
  DG_1,
  DG_2,
  DG_3,
  DG_4,
  DG_5,
  DG_6,
  DG_7,
  DG_8,
  DG_9,
  DG_A,
  DG_B,
  DG_C,
  DG_D,
  DG_E,
  DG_F,
  DG_MINUS,
  DG_H,
  DG_L,
  DG_O,
  DG_R,
  DG_BLANK
} digit_t;

// Hardware interfacing functions - you typically do not need to use the
// next two functions directly

// Send out 8 bits to DIO
// Bits may represent segments of ine digit or a digit address number
void ssld_send_bits(uint8_t bits);

// Latch previously sent bits and illuminate one digit
void ssld_latch_digit(const uint8_t dignum);

// Display functions
// All functions expect a byte buffer where each byte represents a digit

// Cycle over a byte buffer and display one digit each time
// This function must be called periodically (e.g. every 5 ms or so)
void ssld_refresh(uint8_t *buffer);

// Switch an individual display segment off
void ssld_segment_off(
  uint8_t *buffer, const uint8_t dignum, const uint8_t segnum);

// Switch an individual display segment on
void ssld_segment_on(
  uint8_t *buffer, const uint8_t dignum, const uint8_t segnum);

// Set one digit in a buffer
// dignum: 0 - SSLD_SIZE, digits a numbered from right to left
void ssld_set_digit(uint8_t *buffer, const uint8_t dignum, const digit_t digit);

// Render 16 bit integer into a 4-byte display buffer as hexadecimal
void ssld_hexa16(uint8_t *buffer, const uint16_t number);

// Render 16 bit integer into a 4-byte display buffer as decimal (0 - 9999)
void ssld_dec16(uint8_t *buffer, uint16_t number);

// Scale a number from 0-1023 to 0-1000 range
uint16_t ssld_scale_k(uint16_t n);

#endif
